import mido
import argparse
import os
from mido import Message, MetaMessage, MidiFile, MidiTrack
from pprint import pprint


parser = argparse.ArgumentParser()
parser.add_argument('file', help='File to split up')
parser.add_argument('-l', '--segment-length', help='Length of each segment', default=3, type=float)
args = parser.parse_args()
file_path = args.file
SEGMENT_LENGTH = args.segment_length

with open(file_path, "rb") as f:
    mid = MidiFile(file_path)
    current_length = 0
    tempo = 500000
    current_segment = [MetaMessage('set_tempo', tempo = tempo)]
    segments = []

    for msg in mido.merge_tracks(mid.tracks):
        if msg.time > 0:
            time_s = mido.tick2second(msg.time, mid.ticks_per_beat, tempo)
        else:
            time_s = 0

        current_length = current_length + time_s
        if current_length > SEGMENT_LENGTH:
            segments.append(current_segment)
            current_segment = [MetaMessage('set_tempo', tempo = tempo)]
            current_length = time_s

        if msg.type == 'set_tempo':
            tempo = msg.tempo

        current_segment.append(msg)

    segments.append(current_segment)

    # print(len(segments))
    for i, segment in enumerate(segments):
        new_file = MidiFile(ticks_per_beat = mid.ticks_per_beat)
        new_file.tracks.append(MidiTrack(segment))
        (root, ext) = os.path.splitext(file_path)
        new_file.save(f"{root}_{i}{ext}")
