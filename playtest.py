import mido
from mido import Message, MidiFile, MidiTrack
from pprint import pprint

port = mido.open_output()
print("loading file...")
mid = MidiFile("018-049-touhou_inada.mid")
print("starting to play...")
for msg in mid.play():
    port.send(msg)
