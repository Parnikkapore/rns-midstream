import os
import sys
import time
import random
import argparse
import io
import threading
import queue
import RNS
import mido
from mido import Message, MidiFile, MidiTrack

APP_NAME = "midstream"
APP_ASPECTS = ["breakserve"]
BUFFER_SIZE = 2

server_link = None
current_id = 0
max_id = 1
port = mido.open_output()
play_queue = queue.Queue(BUFFER_SIZE)

# This initialisation is executed when the users chooses
# to run as a client
def client(destination_hexhash, configpath, log_level):
    # We need a binary representation of the destination
    # hash that was entered on the command line
    try:
        dest_len = (RNS.Reticulum.TRUNCATED_HASHLENGTH//8)*2
        if len(destination_hexhash) != dest_len:
            raise ValueError(
                "Destination length is invalid, must be {hex} hexadecimal characters ({byte} bytes).".format(hex=dest_len, byte=dest_len//2)
            )

        destination_hash = bytes.fromhex(destination_hexhash)
    except:
        print("Invalid destination entered. Check your input!\n")
        exit()

    # We must first initialise Reticulum
    reticulum = RNS.Reticulum(configpath, loglevel=log_level)

    # Check if we know a path to the destination
    if not RNS.Transport.has_path(destination_hash):
        RNS.log("Destination is not yet known. Requesting path and waiting for announce to arrive...")
        RNS.Transport.request_path(destination_hash)
        while not RNS.Transport.has_path(destination_hash):
            time.sleep(0.1)

    # Recall the server identity
    server_identity = RNS.Identity.recall(destination_hash)

    # Inform the user that we'll begin connecting
    RNS.log("Establishing link with server...")

    # When the server identity is known, we set
    # up a destination
    server_destination = RNS.Destination(
        server_identity,
        RNS.Destination.OUT,
        RNS.Destination.SINGLE,
        APP_NAME,
        *APP_ASPECTS
    )

    # And create a link
    link = RNS.Link(server_destination)

    # We'll set up functions to inform the
    # user when the link is established or closed
    link.set_link_established_callback(link_established)
    link.set_link_closed_callback(link_closed)

    # Everything is set up, so let's enter a loop
    # for the user to interact with the example
    client_loop()

def client_loop():
    global server_link

    # Wait for the link to become active
    while not server_link:
        time.sleep(0.1)

    server_link.request(
        f"manifest",
        data = None,
        response_callback = after_get_manifest,
    )

    play_thread = threading.Thread(target=play_task)
    play_thread.start()
    play_thread.join()
    server_link.teardown()

def after_get_manifest(request_receipt):
    global max_id
    manifest = request_receipt.response
    if manifest['type'] != "static":
        print("Only static streams are supported by this player. Quitting.")
        sys.exit()

    RNS.log("Got manifest!", level=RNS.LOG_VERBOSE)
    current_id = manifest['iframe']
    max_id = manifest['max']

    get_frame()

def get_frame():
    RNS.log(f"Fetching segment {current_id}...", level=RNS.LOG_VERBOSE)
    server_link.request(
        f"{current_id}.mid",
        data = None,
        response_callback = got_response,
        failed_callback = request_failed
    )

def got_response(request_receipt):
    global current_id
    request_id = request_receipt.request_id
    response = request_receipt.response

    total_time_ms = request_receipt.get_response_time() * 1000
    prop_time_ms = server_link.rtt * 1000
    trans_time_ms = total_time_ms - prop_time_ms

    RNS.log(f"Got response for {current_id}: {len(response)} long, " +
            f"in {round(prop_time_ms, 2)}ms prop + {round(trans_time_ms, 2)}ms trans",
            level=RNS.LOG_VERBOSE)

    midi = MidiFile(file=io.BytesIO(response))
    play_queue.put(midi, timeout=60)

    current_id = current_id + 1
    if current_id > max_id:
        RNS.log("Song fetch done!", level=RNS.LOG_VERBOSE)
        play_queue.put(None)
    else:
        get_frame()

def request_failed(request_receipt):
    RNS.log("The request "+RNS.prettyhexrep(request_receipt.request_id)+" failed.")
    get_frame()

def play_task():
    while True:
        midi = play_queue.get(timeout=60)
        if midi == None:
            RNS.log("Song playback done!", level=RNS.LOG_VERBOSE)
            return
        for msg in midi.play():
            port.send(msg)

# This function is called when a link
# has been established with the server
def link_established(link):
    # We store a reference to the link
    # instance for later use
    global server_link
    server_link = link

    # Inform the user that the server is
    # connected
    RNS.log("Link established with server!")

# When a link is closed, we'll inform the
# user, and exit the program
def link_closed(link):
    if link.teardown_reason == RNS.Link.TIMEOUT:
        RNS.log("The link timed out, exiting now")
    elif link.teardown_reason == RNS.Link.DESTINATION_CLOSED:
        RNS.log("The link was closed by the server, exiting now")
    else:
        RNS.log("Link closed, exiting now")
    
    RNS.Reticulum.exit_handler()
    time.sleep(1.5)
    os._exit(0)


##########################################################
#### Program Startup #####################################
##########################################################

# This part of the program runs at startup,
# and parses input of from the user, and then
# starts up the desired program mode.
if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description="Download and play segments in order. PoC version.")

        parser.add_argument(
            "-c",
            "--config",
            action="store",
            default=None,
            help="path to alternative Reticulum config directory",
            type=str
        )

        parser.add_argument(
            "-d",
            "--log-level",
            action="store",
            default=RNS.LOG_NOTICE, # 3
            help="log level (0-7, higher = more verbose)",
            type=int
        )

        parser.add_argument(
            "destination",
            nargs="?",
            default=None,
            help="hexadecimal hash of the server destination",
            type=str
        )

        args = parser.parse_args()

        if args.config:
            configarg = args.config
        else:
            configarg = None

        if (args.destination == None):
            print("")
            parser.print_help()
            print("")
        else:
            client(args.destination, configarg, args.log_level)

    except KeyboardInterrupt:
        print("")
        exit()
