# rns-midstream

A low-bandwidth MIDI streaming experiment.

-----

This is the "but can you play / stream music with it" experiment for [Reticulum (#rnsystem?)](https://reticulum.network/). Reticulum networks are generally pretty low-bandwidth, with the lowest supported bandwidth (common on LoRa networks) being 500 *bits* per second. Yes, you read that right. *bits*.

Opus can cover us on the higher end, all the way down to 6kbps. But we can go lower by using a different way of sending music: MIDI files! By rough calculations, we land at around 2kbps for professionally authored MIDIs, although obviously voice and custom instruments aren't supported and playback quality is soundfont-dependent.

(Xiph says "please just use codec2 if you want to go lower", but music absolutely does not survive going through codec2)

So this is that! Currently I'm toying around with various things - breakserve.py and dlplay.py is likely where the interesting stuff is.
