import mido
import argparse
import io
import os
from mido import Message, MetaMessage, MidiFile, MidiTrack
from pprint import pprint
import RNS

APP_NAME = "midstream"
APP_ASPECTS = ["breakserve"]
# APP_NAME = "nomadnetwork"
# APP_ASPECTS = ["node"]

parser = argparse.ArgumentParser()
parser.add_argument('file', help='File to split up')
parser.add_argument('-l', '--segment-length', help='Length of each segment',
                    default=3, type=float)
parser.add_argument('-d', '--log-level', help='log level (0-7, higher = more verbose)',
                    default=RNS.LOG_NOTICE, type=int)
args = parser.parse_args()
file_path = args.file
SEGMENT_LENGTH = args.segment_length

def main():
    reticulum = RNS.Reticulum(loglevel=args.log_level)
    server_identity = RNS.Identity()
    server_destination = RNS.Destination(
        server_identity,
        RNS.Destination.IN,
        RNS.Destination.SINGLE,
        APP_NAME,
        *APP_ASPECTS
    )
    segbins = segment_binaries(file_path, SEGMENT_LENGTH)
    def make_response_generator(seg):
        return lambda p,d,r,l,i,t: seg
    for i, seg in enumerate(segbins):
        server_destination.register_request_handler(
            f"{i}.mid",
            # `seg` is shared between all iterations.
            # See https://stackoverflow.com/questions/938429/
            response_generator = make_response_generator(seg),
            allow = RNS.Destination.ALLOW_ALL
        )
    server_destination.register_request_handler(
        f"manifest",
        response_generator = lambda p,d,r,i,t: {
            "type": "static",
            "iframe": 0,
            "pframe": 0,
            "max": len(segbins) - 1
        },
        allow = RNS.Destination.ALLOW_ALL
    )
    announce_loop(server_destination)

def announce_loop(destination):
    RNS.log(
        f"{APP_NAME}.{'.'.join(APP_ASPECTS)}" +
        RNS.prettyhexrep(destination.hash) +
        " running, waiting for a connection."
    )

    RNS.log("Hit enter to manually send an announce (Ctrl-C to quit)")
    while True:
        entered = input()
        destination.announce()
        RNS.log("Sent announce from "+RNS.prettyhexrep(destination.hash))

def segment_binaries(file_path, segment_length):
    segbins = []
    with open(file_path, "rb") as f:
        mid = MidiFile(file_path)
        current_length = 0
        tempo = 500000
        current_segment = [MetaMessage('set_tempo', tempo = tempo)]
        segments = []

        for msg in mido.merge_tracks(mid.tracks):
            if msg.time > 0:
                time_s = mido.tick2second(msg.time, mid.ticks_per_beat, tempo)
            else:
                time_s = 0

            current_length = current_length + time_s
            if current_length > segment_length:
                segments.append(current_segment)
                current_segment = [MetaMessage('set_tempo', tempo = tempo)]
                current_length = time_s

            if msg.type == 'set_tempo':
                tempo = msg.tempo

            current_segment.append(msg)

        segments.append(current_segment)

        # print(len(segments))
        for i, segment in enumerate(segments):
            new_file = MidiFile(ticks_per_beat = mid.ticks_per_beat)
            new_file.tracks.append(MidiTrack(segment))
            new_filebuf = io.BytesIO()
            new_file.save(file=new_filebuf)
            segbins.append(new_filebuf.getvalue())
            new_filebuf.close()
    return segbins

main()
